from collections import defaultdict 
import os 
import random 
import argparse


###########################################################
#### Select the BEST identical PDB candiate ###############
#### 1. Raw filter with iden threshold 2-way ##############
####   iden-seq > 0.7 && iden-PDB >0.95 ###################
#### 2. Pick up the highest iden LIST #####################
#### 3. Random choose One Chain if Tier ###################
###########################################################

#### Set args parameters
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--infile', type=str,
                    help = 'input raw identity csv file')
parser.add_argument('-o', '--outfile', type=str,
                    help = 'output Fully-covered PDB info csv file')
args = parser.parse_args()
in_raw_file = args.infile
out_iden_csv = args.outfile


#### Select the entry including identical PDB
def SelectCandidate(raw_identity_file,
                    out_selection_csv_file,
                    iden_threshold_seq,
                    iden_threshold_pdb):
    
    ## Raw filtering, HASH uniprotHEADER =>MaxIdentity
    dict_seq_maxIden = {}

    f = open(raw_identity_file, 'r')
    for line in f:
        line = line.strip()
        list_line_slices = line.split(',')
        iden_against_seq = list_line_slices[2].strip()
        iden_against_pdb = list_line_slices[3].strip()

        seq1_whole_header = list_line_slices[0].strip()
        pdb_header = list_line_slices[1].strip()

        iden_against_seq = float(iden_against_seq)
        iden_against_pdb = float(iden_against_pdb)
        sum_identity = iden_against_seq + iden_against_pdb

        if iden_against_seq >= iden_threshold_seq:
            if iden_against_pdb >= iden_threshold_pdb:
                ## Check whether key existing in dictionary 
                if seq1_whole_header in dict_seq_maxIden:
                    old_value = dict_seq_maxIden[seq1_whole_header]
                    if old_value < sum_identity:
                        dict_seq_maxIden[seq1_whole_header] = sum_identity
                else:
                    dict_seq_maxIden [seq1_whole_header] = sum_identity
    f.close()

    ## Obtain HASH uniprotHEADER => List of Highest Iden PDB
    dict_seq_listBestpdbs = defaultdict(list)

    f = open(raw_identity_file, 'r')
    for line in f:
        line = line.strip()
        list_line_slices = line.split(',')
        iden_against_seq = list_line_slices[2].strip()
        iden_against_pdb = list_line_slices[3].strip()

        seq1_whole_header = list_line_slices[0].strip()
        pdb_header = list_line_slices[1].strip()

        iden_against_seq = float(iden_against_seq)
        iden_against_pdb = float(iden_against_pdb)
        sum_identity = iden_against_seq + iden_against_pdb

        ## discard low-iden uniprot
        try:
            if sum_identity == dict_seq_maxIden [seq1_whole_header]:
                # print(line)
                dict_seq_listBestpdbs[seq1_whole_header].append(pdb_header)
        except:
            pass
    f.close()
    
    ## Randomly choose One PDB chain
    out_F = open(out_iden_csv, 'w')
    outF_header = 'queryID,pdbchainID,identityQuery,identityPDB,totalLength,coverageQuery,coveragePDB' + "\n"
    out_F.write(outF_header)
    
    for uniprotHeader, list_best_pdb_candidates in dict_seq_listBestpdbs.items():
        selected_one_pdb = random.choice(list_best_pdb_candidates)

        prefix_header = uniprotHeader + ',' + selected_one_pdb
        
        f = open(raw_identity_file, 'r')
        for line in f:
            if line.startswith(prefix_header):
                out_F.write(line)
        f.close()

        for each_candidate in list_best_pdb_candidates:
            redundant_uniprot_pdb = uniprotHeader + ',' + selected_one_pdb
            print(redundant_uniprot_pdb)

        ## Copy the aligned pair to de-signated folder         
        # file_prefix = uniprot_ID + '_' + pdb_chain_ID
        # for root, subdirs, files in os.walk("/home/bwang/tmp/alignedpair/"):
        #     for each_file in files:
        #         if each_file.startswith(file_prefix):
        #             old_path = root + each_file
        #             new_path = "/home/bwang/tmp/selectaigned/"
        #             cp_cmd = 'cp' + "\t" + old_path + "\t" + new_path
        #             os.system(cp_cmd)

    out_F.close()
    return 0

SelectCandidate(raw_identity_file = in_raw_file,
                out_selection_csv_file = out_iden_csv ,
                iden_threshold_seq = 0.7,
                iden_threshold_pdb = 0.95)